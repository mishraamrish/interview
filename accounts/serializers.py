from django.core.validators import EmailValidator
from pytz import all_timezones
from django.contrib.auth import get_user_model
from django.db.utils import IntegrityError
from rest_framework import serializers
from rest_framework.serializers import SerializerMethodField
from .models import ActivityPeriod

User = get_user_model()


class ActivitySerializer(serializers.ModelSerializer):

    class Meta:
        model = ActivityPeriod
        fields = ("start_time", "end_time")


class UserSerializer(serializers.ModelSerializer):
    real_name = SerializerMethodField()
    activity_period = ActivitySerializer(many=True)

    class Meta:
        model = User
        fields = ("id", "tz", "real_name", "activity_period")

    def get_real_name(self, obj):
        return f"{obj.first_name} {obj.last_name}"


class UserRegistrationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[EmailValidator()], required=True)
    password = serializers.CharField(style={'input_type': 'password'},
                                     max_length=128, required=True,
                                     write_only=True)
    confirm_password = serializers.CharField(max_length=128, write_only=True)
    tz = serializers.CharField(max_length=128, write_only=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password', 'confirm_password', 'tz')

    @staticmethod
    def get_confirm_password(instance):
        return

    def create(self, data):
        data.pop('confirm_password', None)
        try:
            user = User.objects.create_user(**data)
            return user
        except IntegrityError:
            raise serializers.ValidationError('User with same email already exist')
        except Exception as e:
            raise serializers.ValidationError(f'{str(e)} error occurred please try again after some time')

    def validate(self, attrs):
        confirm_password = attrs.get('confirm_password')
        if attrs.get('password') != confirm_password:
            raise serializers.ValidationError('Password and Confirm Password do not match.')
        if attrs.get('tz') not in all_timezones:
            raise serializers.ValidationError('Password and Confirm Password do not match.')
        return attrs

