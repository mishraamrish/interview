import uuid
from django.db.models.signals import pre_save, post_delete, pre_delete
from django.dispatch import receiver
from pytz import all_timezones
from random import sample
from string import ascii_uppercase, digits
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import EmailValidator
from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.sessions.models import Session


def generate_id(uid):
    # import ipdb ; ipdb.set_trace()
    # last_user = User.objects.all().order_by('uid').last()
    middle_string = "".join(sample(f"{ascii_uppercase}{digits}", 7))
    # if not last_user:
    #     return f"W{middle_string}{uid}"
    # user_id = last_user.pk
    # user_int = int(user_id[8:])
    # new_user_int = user_int + 1
    new_user_id = f"W{middle_string}{uid}"
    return new_user_id


class CustomUserManager(UserManager):

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        user.id = generate_id(user.uid)
        user.save()
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **extra_fields)


class ActivityPeriod(models.Model):
    # user = models.ForeignKey(User, related_name="activity_periods", null=False, blank=False, on_delete=models.PROTECT)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()


class User(AbstractUser):
    TZ_CHOICES = [(x, x) for x in all_timezones]
    """id = models.UUIDField(
        max_length=20,
        primary_key=True,
        default=uuid.uuid4,
        editable=False)"""
    uid = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='UID')
    id = models.CharField(max_length=20, null=True)
    username = None
    email = models.EmailField(
        _('email address'),
        blank=False, null=False,
        unique=True,
        validators=[EmailValidator()]
    )
    tz = models.CharField(max_length=56, default="UTC", choices=TZ_CHOICES, null=False, blank=False)
    activity_period = models.ManyToManyField(ActivityPeriod, related_name="users")
    objects = CustomUserManager()
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []


# @receiver(pre_delete, sender=Session)
# def user_limit_check(sender, instance, **kwargs):
#     data = instance.get_decoded()
#     user_id = data.get("_auth_user_id")
#     if user_id:
#         start_time = data.get("start_time")
#         end_time = data.get("end_time")
#         user = User.objects.get(pk=user_id)
#         user.activity_period.create(start_time=start_time, end_time=end_time)
