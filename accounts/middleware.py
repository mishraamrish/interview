from datetime import datetime
from django.utils.deprecation import MiddlewareMixin
from .models import ActivityPeriod


class ActivityMiddleware(MiddlewareMixin):

    def process_request(self, request):
        # import ipdb; ipdb.set_trace()
        # print(request.session.items())
        if request.user.is_authenticated:
            activity_id = request.session.get("activity_id")
            if not activity_id:
                user = request.user
                activity_period = ActivityPeriod(
                    start_time=datetime.now(),
                    end_time=datetime.now()
                )
                activity_period.save()
                user.activity_period.add(activity_period)
            else:
                activity_period = ActivityPeriod.objects.get(id=activity_id)
                activity_period.end_time = datetime.now()
                activity_period.save()
            request.session["activity_id"] = activity_period.pk
