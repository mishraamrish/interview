from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .models import User, ActivityPeriod
from .serializers import UserSerializer


class UserActivityViewSet(viewsets.ModelViewSet):
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    # permission_classes = [
    #     IsAuthenticated,
    # ]
