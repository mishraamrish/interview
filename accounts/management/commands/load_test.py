import uuid
from pytz import all_timezones
from datetime import datetime, timedelta
from random import randint, choice
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from utils.names import dummy_names
from accounts.models import ActivityPeriod
User = get_user_model()


class Command(BaseCommand):
    help = 'Load Test Data For Testing'

    def handle(self, *args, **kwargs):
        for first_name, last_name in dummy_names:
            email = f"{str(uuid.uuid4()).replace('-', '')}@test.com"
            password = "BeautifulLife"
            user = User.objects.create_user(
                first_name=first_name,
                last_name=last_name,
                password=password,
                email=email,
                tz=choice(all_timezones)
            )
            for i in range(randint(1, 7)):
                activity = ActivityPeriod(
                    start_time=datetime.now() - timedelta(
                        days=randint(1, 365),
                        hours=randint(0, 23),
                        minutes=randint(0, 59)
                    ),
                    end_time=datetime.now() - timedelta(
                        days=randint(1, 365),
                        hours=randint(0, 23),
                        minutes=randint(0, 59)
                    )
                )
                activity.save()
                user.activity_period.add(activity)
