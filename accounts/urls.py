from django.conf.urls import url, include
from .views import UserActivityViewSet


urlpatterns = [
    url(r'^activity', UserActivityViewSet.as_view({"get": "list"}))
]