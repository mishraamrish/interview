from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six
UserModel = get_user_model()


class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )


account_activation_token = TokenGenerator()


# class EmailBackend(ModelBackend):
#     def authenticate(self, username=None, password=None, **kwargs):
#         try:
#             user = UserModel.objects.get(email=kwargs.get('email'))
#         except UserModel.DoesNotExist:
#             raise ValueError("User does not exist")
#         else:
#             if user.check_password(password):
#                 return user
#             else:
#                 raise ValueError("Incorrect password")
