>**Logics for start and end time**  
     
I have created Middleware (in `accounts.middleware`) and one supporting ActivityPeriod Model Which is ForeignKey(m2m) to Custom User Model.

>**Rest API**

I have written API using Django rest framework, viewsets, serializer and pagination to mange user object with limit of 50 objects per page.

>**Custom command to load Test/Development data**

Path to the command script is `acconts.management.commands.load_test`


>**Test User**

`email: amrish@test.com`
`password: test@123`

I have not covered the Authentication completely.
I have already done Custom Authentication with multiple User role
please refer 

**Backend**: https://bitbucket.org/mishraamrish/authors-backend/

**FrontEnd**: https://bitbucket.org/mishraamrish/authors/